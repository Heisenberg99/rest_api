import logging
import argparse

# logger configs
def setup_logger():
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter(
        '%(asctime)s:%(name)s:%(levelname)s:%(message)s')
    file_handler = logging.FileHandler('log/api.log')
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)

    return logger

# parser configs
def arg_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("--port", type=int, default=5000, help="Port number must have integer type")
    args = parser.parse_args()
    return args
