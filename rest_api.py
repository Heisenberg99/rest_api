from flask import Flask, jsonify, make_response, request
from model import update_model_pipeline, get_pred, load_model
import sqlalchemy as db
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy.ext.declarative import declarative_base
from flask_jwt_extended import JWTManager, jwt_required, get_jwt_identity
from config import SECRET_KEY, STORAGE_NAME
from utils import setup_logger, arg_parser
from apispec.ext.marshmallow import MarshmallowPlugin
from apispec import APISpec
from flask_apispec.extension import FlaskApiSpec
from schemas import PredictionSchema, UserSchema, AuthSchema, ModelUpdateSchema, GetPredictionSchema
from flask_apispec import use_kwargs, marshal_with


# Load machine learning model
model = load_model()

# Setup logger
logger = setup_logger()

# Create Flask application configs
app = Flask(__name__)
app.config['SECRET_KEY'] = SECRET_KEY
# The create_engine() function produces an Engine object based on a URL
engine = create_engine(STORAGE_NAME)
# # configure Session class with desired options,  scoped_session() for thread safety
session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=engine))
# declarative_base() is a factory function that constructs a base class for declarative class definitions
Base = declarative_base()
Base.query = session.query_property()
# This object is used to hold the JWT settings and callback functions
jwt = JWTManager(app)

docs = FlaskApiSpec()
docs.init_app(app)

app.config.update({
    'APISPEC_SPEC': APISpec(
        title='ML project',
        version='v1',
        openapi_version='2.0',
        plugins=[MarshmallowPlugin()],
    ),
    'APISPEC_SWAGGER_URL': '/swagger/'
})


# Create a table
from database import *
Base.metadata.create_all(bind=engine)


@app.route('/all_predicts', methods=['GET'])
@jwt_required()
@marshal_with(PredictionSchema(many=True))
def get_all_predicts():
    try:
        user_id = get_jwt_identity()
        if user_id == 1:
            predictions = Prediction.query.all()
        else:
            predictions = Prediction.query.filter(Prediction.user_id == user_id).all()
    except Exception as e:
        logger.warning(f'all_predicts - create action failed with errors: {e}')
        return {'message': str(e)}, 400

    return predictions


@app.route('/all_predicts/<int:predict_id>', methods=['DELETE'])
@jwt_required()
@marshal_with(PredictionSchema)
def delete_predict(predict_id):
    try:
        user_id = get_jwt_identity()
        if user_id == 1:
            prediction = Prediction.query.filter(Prediction.id == predict_id).all()
        else:
            prediction = Prediction.query.filter(Prediction.id == predict_id,
                                                 Prediction.user_id == user_id).all()
        if not prediction:
            return {'message': 'No predicts with this id'}, 400
        session.delete(prediction)
        session.commit()
    except Exception as e:
        logger.warning(f'delete_predict with predict_id:{predict_id} - create action failed with errors: {e}')
        return {'message': str(e)}, 400

    return {'message': 'predict with this id successfully deleted'}, 204


@app.route('/all_predicts/<int:predict_id>', methods=['GET'])
@jwt_required()
@marshal_with(PredictionSchema)
def find_predict(predict_id):
    try:
        user_id = get_jwt_identity()
        if user_id == 1:
            prediction = Prediction.query.filter(Prediction.id == predict_id).all()
        else:
            prediction = Prediction.query.filter(Prediction.id == predict_id,
                                                 Prediction.user_id == user_id).all()
        if not prediction:
            return {'message': 'No predicts with this id'}, 400
    except Exception as e:
        logger.warning(f'find_predict with predict_id:{predict_id} - create action failed with errors: {e}')
        return {'message': str(e)}, 400

    return prediction, 204


@app.route('/last_predict', methods=['GET'])
@jwt_required()
@marshal_with(PredictionSchema)
def get_last_predict():
    try:
        user_id = get_jwt_identity()
        if user_id == 1:
            predictions = Prediction.query.all()
        else:
            predictions = Prediction.query.filter(Prediction.user_id == user_id).all()
        if not predictions:
            return {'message': 'no predictions'}, 200
        predictions = predictions[-1]
    except Exception as e:
        logger.warning(f'get_last_predict - create action failed with errors: {e}')
        return {'message': str(e)}, 400

    return predictions


@app.route('/last_predict', methods=['DELETE'])
@jwt_required()
@marshal_with(PredictionSchema)
def delete_last_predict():
    try:
        user_id = get_jwt_identity()
        if user_id == 1:
            predictions = Prediction.query.all()
        else:
            predictions = Prediction.query.filter(Prediction.user_id == user_id).all()
        if not predictions:
            return {'message': 'No predictions'}, 400
        session.delete(predictions[-1])
        session.commit()
    except Exception as e:
        logger.warning(f'delete_last_predict with last predict - create action failed with errors: {e}')
        return {'message': str(e)}, 400

    return {'message': 'last predict successfully deleted'}, 204


@app.route('/predict', methods=['GET'])
@use_kwargs(GetPredictionSchema)
@marshal_with(GetPredictionSchema)
@jwt_required()
def get_predict(**kwargs):
    try:
        user_id = get_jwt_identity()
        new_elem = Prediction(user_id=user_id, **kwargs)
    except Exception as e:
        logger.warning(f'get_predict - create action failed with errors: {e}')
        return {'message': str(e)}, 400

    return new_elem


@app.route('/predict', methods=['POST'])
@use_kwargs(PredictionSchema)
@marshal_with(PredictionSchema)
@jwt_required()
def post_predict(**kwargs):
    try:
        user_id = get_jwt_identity()
        new_elem = Prediction(user_id=user_id, **kwargs)
        session.add(new_elem)
        session.commit()
    except Exception as e:
        logger.warning(f'post_predict - create action failed with errors: {e}')
        return {'message': str(e)}, 400

    return new_elem

# model correct
@app.route('/update_model', methods=['POST'])
@jwt_required()
@use_kwargs(ModelUpdateSchema)
def update_model(**kwargs):
    try:
        user_id = get_jwt_identity()
        if user_id == 1:
            accuracy = update_model_pipeline(**kwargs)
        else:
            logger.warning(f'User with user_id: {user_id} has no agreements to update the model')
            return {'message': 'Permission error'}
    except Exception as e:
        logger.warning('Error in train pipeline')
        return {'message': str(e)}, 400

    return {'accuracy': accuracy}, 200

# personal
@app.route('/register', methods=['POST'])
@use_kwargs(UserSchema)
@marshal_with(AuthSchema)
def register(**kwargs):
    try:
        user = User(**kwargs)
        session.add(user)
        session.commit()
        token = user.get_token()
    except Exception as e:
        logger.warning(f'registration failed with errors: {e}')
        return {'message': str(e)}, 400

    return {'access_token': token}


@app.route('/login', methods=['POST'])
@use_kwargs(UserSchema(only=('email', 'password')))
@marshal_with(AuthSchema)
def login(**kwargs):
    try:
        user = User.authenticate(**kwargs)
        token = user.get_token()
    except Exception as e:
        logger.warning(f'login with email {kwargs["email"]} failed with errors: {e}')
        return {'message': str(e)}, 400

    return {'access_token': token}


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'code': 'PAGE_NOT_FOUND'}), 404)


@app.errorhandler(500)
def server_error(error):
    return make_response(jsonify({'code': 'INTERNAL_SERVER_ERROR'}), 500)


@app.teardown_appcontext
def shutdown_session(exception=None):
    session.remove()


docs.register(get_all_predicts)
docs.register(delete_predict)
docs.register(find_predict)
docs.register(get_last_predict)
docs.register(delete_last_predict)
docs.register(post_predict)
docs.register(update_model)
docs.register(register)
docs.register(login)


if __name__ == '__main__':
    args = arg_parser()
    port = args.port
    app.run(port=port, debug=True)
