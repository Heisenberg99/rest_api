from marshmallow import Schema, validate, fields
from config import MAX_NAME_LENGTH, MAX_EMAIL_LENGTH, MAX_PASSWORD_LENGTH


class GetPredictionSchema(Schema):
    sepal_length = fields.Float(required=True)
    sepal_width = fields.Float(required=True)
    petal_length = fields.Float(required=True)
    petal_width = fields.Float(required=True)
    message = fields.String(dump_only=True)


class PredictionSchema(Schema):
    id = fields.Integer(dump_only=True)
    user_id = fields.Integer(dump_only=True)
    sepal_length = fields.Float(required=True)
    sepal_width = fields.Float(required=True)
    petal_length = fields.Float(required=True)
    petal_width = fields.Float(required=True)
    setosa_pred = fields.Float(required=True)
    versicolor_pred = fields.Float(required=True)
    virginica_pred = fields.Float(required=True)
    message = fields.String(dump_only=True)


class TestPredictionSchema(Schema):
    sepal_length = fields.Float(required=True)
    sepal_width = fields.Float(required=True)
    petal_length = fields.Float(required=True)
    petal_width = fields.Float(required=True)


class ModelUpdateSchema(Schema):
    n_estimators = fields.Integer(required=True, validate=[validate.Range(min=1)])
    max_depth = fields.Integer(required=True, validate=[validate.Range(min=1)])
    criterion = fields.Str(required=True, validate=[validate.ContainsOnly(['gini', 'entropy'])])


class UserSchema(Schema):
    name = fields.String(required=True, validate=[validate.Length(max=MAX_NAME_LENGTH)])
    email = fields.String(required=True, validate=[validate.Length(max=MAX_EMAIL_LENGTH)])
    password = fields.String(required=True, validate=[validate.Length(max=MAX_PASSWORD_LENGTH)], load_only=True)
    predictions = fields.Nested(PredictionSchema, many=True, dump_only=True)


class AuthSchema(Schema):
    access_token = fields.String(dump_only=True)
    message = fields.String(dump_only=True)
