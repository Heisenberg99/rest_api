import os
# криптографическая соль
'''
import uuid
uuid.uuid4().hex
'''
SECRET_KEY = str(os.getenv('SECRET_KEY', '4529ab8231134bbd8cfd309b7fef1d1e'))
MODEL_NAME = str(os.getenv('MODEL_NAME', 'model.pkl'))
OLD_MODEL_FOLDER = str(os.getenv('OLD_MODEL_FOLDER', 'old_model/'))
NEW_MODEL_FOLDER = str(os.getenv('NEW_MODEL_FOLDER', 'new_model/'))
STORAGE_NAME = str(os.getenv('STORAGE_NAME', 'sqlite:///sqlite.db'))
CURRENT_MODEL = NEW_MODEL_FOLDER + MODEL_NAME

MAX_NAME_LENGTH = 200
MAX_EMAIL_LENGTH = 200
MAX_PASSWORD_LENGTH = 100
