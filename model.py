import pickle
import pandas as pd
import os
import shutil
from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn import metrics
from config import MODEL_NAME, OLD_MODEL_FOLDER, NEW_MODEL_FOLDER, CURRENT_MODEL


def load_model():
    return pickle.load(open(CURRENT_MODEL, 'rb'))


def data_gathering():
    return datasets.load_iris()


def data_preprocessing(data):
    data = pd.DataFrame({
        'sepal length': data.data[:, 0],
        'sepal width': data.data[:, 1],
        'petal length': data.data[:, 2],
        'petal width': data.data[:, 3],
        'species': data.target
    })
    X = data[['sepal length', 'sepal width', 'petal length', 'petal width']]
    y = data['species']
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3)

    return X_train, X_test, y_train, y_test


def train_model(X_train, y_train, X_test, y_test, n_estimators, max_depth, criterion):
    clf = RandomForestClassifier(n_estimators=n_estimators,
                                 max_depth=max_depth,
                                 criterion=criterion)
    clf.fit(X_train, y_train)
    y_pred = clf.predict(X_test)
    accuracy = metrics.accuracy_score(y_test, y_pred)

    return clf, accuracy

def save_model(clf):
    if not os.path.exists(OLD_MODEL_FOLDER):
        os.makedirs(OLD_MODEL_FOLDER)
    if not os.path.exists(NEW_MODEL_FOLDER):
        os.makedirs(NEW_MODEL_FOLDER)
    # filenames
    version_num = len(os.listdir(OLD_MODEL_FOLDER))
    new_model_name = MODEL_NAME
    if version_num:
        old_model_name = f'model_{version_num}.pkl'
        # move old model
        shutil.copy(NEW_MODEL_FOLDER + new_model_name, OLD_MODEL_FOLDER)
        # delete processed file from input_dir
        os.chmod(NEW_MODEL_FOLDER + new_model_name, 0o777)
        os.remove(NEW_MODEL_FOLDER + new_model_name)
        # rename old model
        os.rename(OLD_MODEL_FOLDER + new_model_name, OLD_MODEL_FOLDER + old_model_name)
        # save new model
        pickle.dump(clf, open(NEW_MODEL_FOLDER + new_model_name, 'wb'))
    else:
        pickle.dump(clf, open(NEW_MODEL_FOLDER + new_model_name, 'wb'))


def update_model_pipeline(n_estimators, max_depth, criterion):
    data = data_gathering()
    X_train, X_test, y_train, y_test = data_preprocessing(data)
    clf, accuracy = train_model(X_train, y_train, X_test, y_test, n_estimators, max_depth, criterion)
    save_model(clf)

    return accuracy

# get model prediction
def get_pred(sepal_length, sepal_width, petal_length, petal_width, model):
    all_columns = ['sepal length', 'sepal width', 'petal length', 'petal width']
    lst = [sepal_length, sepal_width, petal_length, petal_width]
    input_df = pd.DataFrame([lst], columns=all_columns)
    result = model.predict_proba(input_df)
    preds = ['%.3f' % elem for elem in result[0]]

    setosa_pred = preds[0]
    versicolor_pred = preds[1]
    virginica_pred = preds[2]

    return setosa_pred, versicolor_pred, virginica_pred
