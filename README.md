open cmd and go to "api" folder (like "cd C:\api")
make python venv through command `python -m venv venv`
update pip ` .\venv\scripts\python.exe -m pip install --upgrade pip`
activate python venv ".\venv\Scripts\activate.bat" -> expected to see "(venv)" before command line
install python packages "pip install -r requirements.txt"
run api server
set PYTHONPATH=%PYTHONPATH%;.
set PYTHONHASHSEED=42
python \app.py
